import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'playlist-form',
  template: `
  <div class="card" >
  <div class="card-block">
    <h4 class="card-title">Tytuł playlisty</h4>
    <p class="card-text">Opis</p>
  </div>
  <div class="card-block">
    <div class="form-group" [ngClass]="{'not-empty-input': emptyInput == false}">
      <input [(ngModel)]="playlist.name" type="text" class="form-control">
      <label>Nazwa:</label>
    </div>
    <div class="form-group" [ngClass]="{'not-empty-input': emptyInput == false}">
      <input [value]="playlist.tracks + ' utworów'" disabled type="text" class="form-control">
      <label>Utwory:</label>
    </div>
    <div class="form-group">
      <label for="color">Kolor:</label>
      <input [(ngModel)]="playlist.color" type="color" class="" id="color">
    </div>
    <div class="form-group">
      <label for="favourite">Ulubione</label>
      <input [(ngModel)]="playlist.favourite" type="checkbox" id="favourite">
    </div>
    <div class="form-group">
      <button (click)="save($event)" class="btn btn-success float-right">Zapisz</button>
    </div>
  </div>

</div>
  `,
  inputs: [
    'playlist'
  ],
  styleUrls: ['./playlist-form.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistFormComponent implements OnInit {
  emptyInput = false;

  @Input()
  playlist;

  save(event) {
    console.log('Zapisano', event);
  }

  setEmptyInput(event) {
    if (event.target.value !== '') {
      this.emptyInput = true;
    } else {
      this.emptyInput = false;
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
