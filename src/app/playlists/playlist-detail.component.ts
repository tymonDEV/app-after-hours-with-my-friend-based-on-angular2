import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'playlist-detail',
  template: `
  <div class="card" >
  <div class="card-block">
    <h4 class="card-title">Playlista - {{playlist.name}}</h4>
    <p class="card-text">Wybrana Playlista</p>
  </div>
  <div class="card-block">
    <div class="form-group">
      <button (click)="edit(playlist)" class="btn btn-success float-right">Edytuj</button>
    </div>
  </div>
</div>
  `,
  styles: []
})
export class PlaylistDetailComponent implements OnInit {

  @Output('editplaylist')
  onEdited = new EventEmitter();

  @Input()
  playlist;

  edit(playlist) {
    this.onEdited.emit(playlist);
  }

  constructor() { }

  ngOnInit() {
  }

}
