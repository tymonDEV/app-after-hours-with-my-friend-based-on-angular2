import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'content-card',
  template: `
  <div class="card">
  <div class="card-block">
    <h4 class="card-title">{{title}}</h4>
    <p class="card-text">{{content}}</p>
  </div>
  `,
  inputs: [
    'title',
    'content'
  ],
  styles: []
})
export class ContentCardComponent implements OnInit {

  @Input()
  title = 'Playlista';

  @Input()
  content = 'Wybierz Playlistę';

  constructor() { }

  ngOnInit() {
  }

}
