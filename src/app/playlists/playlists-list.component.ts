import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'playlists-list',
  template: `
  <table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Nazwa</th>
      <th>Utworów</th>
      <th>Ulubiona</th>
    </tr>
  </thead>
  <tbody>
    <tr *ngFor="let playlist of playlists; let i = index" class="playlist-row" (click)="select(playlist)" [ngClass]="{'table-active': selected == playlist}"
      [ngStyle]="{borderBottomColor: playlist.color}">
      <td>{{i + 1}}</td>
      <td>{{playlist.name}}</td>
      <td>{{playlist.tracks}}</td>
      <td>
        <label for="favourite">Ulubione</label>
        <input (click)="$event.stopPropagation()" [(ngModel)]="playlist.favourite" type="checkbox" id="favourite">
      </td>
    </tr>
  </tbody>
</table>
  `,
  styles: []
})
export class PlaylistsListComponent implements OnInit {

  @Output('selected')
  onSelected = new EventEmitter();

  @Input()
  playlists;

  @Input()
  selected;

  select(playlist) {
    this.onSelected.emit(playlist);
  }

  constructor() { }

  ngOnInit() {
  }

}
