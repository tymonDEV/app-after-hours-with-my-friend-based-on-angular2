import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  

  selected = null;

  mode = 'none';

  playlists = [
    {
      name: 'The best of Tymon Styles',
      tracks: 23,
      color: '#fff000',
      favourite: true
    },
    {
      name: 'The best of Art Penion',
      tracks: 4,
      color: '#222222',
      favourite: false
    }
  ];

  edited = {

  };

  select(playlist) {
    if (playlist !== this.selected) {
      this.mode = 'selected';
      this.selected = playlist;
    }
  }

  edit(playlist) {
    this.mode = 'edit';
    this.edited = playlist;
    this.selected = playlist;
  }

  createNew() {
    this.mode = 'new';
    const newPlaylist = {};
    this.selected = newPlaylist;
    this.edited = newPlaylist;
  }

  getPlaylistStyle(playlist) {
    return {
      borderBottomColor: playlist.color,
      borderBottomWidth: '4px'
    };
  }

  constructor() { }

  ngOnInit() {
  }

}
